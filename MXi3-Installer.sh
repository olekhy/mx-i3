#!/bin/bash
read -p "Please enter your username: " name

# Update Repositories
sudo apt-get update -y

# Copy relevant Configuration Files
mv MX-Scripts ~/
cp ~/MX-Scripts/conky-i3bar ~/ && cp ~/MX-Scripts/.conkyrci3 ~/ && cp -Rf ~/MX-Scripts/.config ~/ && cp ~/MX-Scripts/.bashrc ~/
cp -Rf ~/MX-Scripts/.epsxe ~/ && cp -Rf ~/MX-Scripts/.fonts ~/ && cp -Rf ~/MX-Scripts/.urxvt ~/
cp -Rf ~/MX-Scripts/.vimrc ~/ && cp -Rf ~/MX-Scripts/.Xresources ~/ && cp -Rf ~/MX-Scripts/.xinitrc ~/ && cp ~/MX-Scripts/Minecraft.jar ~/

# Copy same files to etc/skel
sudo cp ~/MX-Scripts/conky-i3bar /etc/skel/ && sudo cp ~/MX-Scripts/.conkyrci3 /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.config /etc/skel/
sudo cp -Rf ~/MX-Scripts/.epsxe /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.fonts /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.urxvt /etc/skel/
sudo cp -Rf ~/MX-Scripts/.vimrc /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.Xresources /etc/skel/ && sudo cp -Rf ~/MX-Scripts/.xinitrc /etc/skel/
sudo cp -Rf ~/MX-Scripts/.bashrc /etc/skel/ && sudo cp ~MX-Scripts/Minecraft.jar /etc/skel/

# Installing Virtualisation
sudo apt install virtualbox-guest-utils virtualbox-guest-dkms virtualbox-guest-additions-iso open-vm-tools open-vm-tools-desktop dkms build-essential linux-headers-$(uname -r) -y

# Instlling Java 8 for Minecraft
sudo apt-get install openjdk-8-jdk openjdk-8-jdk-headless openjdk-8-jre openjdk-8-jre-headless openjdk-8-source -y
# Install i3-System and programs
sudo apt-get update -y
sudo apt-get install i3status i3blocks rofi nitrogen compton galternatives rxvt-unicode vim vim-syntax-gtk steam asunder aac-enc tmux ranger ncmpcpp mpd libmpdclient2 mpv lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings -y
sudo rm /etc/mpd.conf
sudo apt-get install radiotray w3m w3m-img -y

# install i3-gaps dependencies 
sudo apt-get install git gcc make dh-autoreconf libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev -y
sudo apt-get install libxcb-icccm4-dev libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev  -y
sudo apt-get install libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev -y
sudo apt-get install libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev libxcb-shape0-dev  -y

# clone the repository
cd ~/
git clone https://www.github.com/Airblader/i3 i3-gaps
cd i3-gaps

# compile & install
autoreconf --force --install
rm -rf build/
mkdir -p build && cd build/

# Disabling sanitizers is important for release versions!
# The prefix and sysconfdir are, obviously, dependent on the distribution.
../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers
make
sudo make install

# install polybar dependencies 
sudo apt-get install clang cmake cmake-data git pkg-config python3 python3-sphinx libcairo2-dev -y 
sudo apt-get install libxcb1-dev libxcb-util0-dev libxcb-randr0-dev libxcb-composite0-dev -y
sudo apt-get install python-xcbgen xcb-proto libxcb-image0-dev libxcb-ewmh-dev libxcb-icccm4-dev -y
sudo apt-get install libxcb-xkb-dev libxcb-xrm-dev libxcb-cursor-dev libasound2-dev libpulse-dev -y 
sudo apt-get install libjsoncpp-dev libmpdclient-dev libcurl4-openssl-dev libnl-genl-3-dev -y

# Compile Polybar
cd ~/
# Make sure to type the `git' command as is to clone all git submodules too
git clone --recursive https://github.com/polybar/polybar
cd polybar
mkdir build
cd build
cmake ..
make -j$(nproc)
# Optional. This will install the polybar executable in /usr/local/bin
sudo make install

# Remove libreoffice
sudo apt remove -y libnumbertext-1.0-0 libnumbertext-data libreoffice-avmedia-backend-gstreamer libreoffice-base libreoffice-base-core 
sudo apt remove -y libreoffice-base-drivers libreoffice-calc libreoffice-common libreoffice-core libreoffice-draw libreoffice-gnome libreoffice-gtk3 
sudo apt remove -y libreoffice-help-common libreoffice-help-en-us libreoffice-impress libreoffice-java-common libreoffice-math libreoffice-report-builder-bin 
sudo apt remove -y libreoffice-sdbc-hsqldb libreoffice-style-colibre libreoffice-style-tango libreoffice-writer lo-main-helper mugshot mythes-en-us 
sudo apt remove -y printer-driver-cups-pdf uno-libs3 ure
#sudo reboot
#sudo apt-get update && sudo apt-get upgrade

# Add Polybar Configs
sudo rm -Rf ~/i3-gaps
sudo rm -Rf ~/polybar 
sudo chmod +x ~/.config/polybar/launch.sh
sudo apt-get update -y


# Add IceSSB
sudo apt install gdebi devscripts
cd ~/MX-i3
sudo gdebi ice_5.3.0_all.deb
sudo sed -i 's/#autologin-session=UNIMPLEMENTED/autologin-session=i3/g' /etc/lightdm/lightdm.conf
sudo sed -i "s/autologin-user=demo/autologin-user=$name/g" /etc/lightdm/lightdm.conf
sudo rm -Rf ~/MX-Scripts && sudo rm -Rf ~/mx-i3

minstall-pkexec &